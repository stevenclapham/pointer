# Pointer
This module adds the ability for each user to show a cursor following his mouse as well as adding the option to ping a certain location.  

## Installation
Download the [Zip file](https://gitlab.com/moerills-fvtt-modules/pointer/raw/master/pointer.zip?inline=false) and extract it into your ``public/modules`` folder. Don't forget to restart FVTT and activating this module!

## How To
While pressing ``X`` your cursor position will be send to  all other connected players.  
If you press the left mouse-button while pressing ``X`` you send a *ping* to every connected player.  
### For Gamemasters only:
If you press the right mouse-button while pressing ``X`` you send a *ping* to every connected player *and* force their camera to the pings position.

### Important
The pings and cursors will only be shown to players on the same scene!

## Customization options
You are able to  customize  the cursor used or the ping used.

### Changing the cursor
Swap out the ``cursor.svg`` for an ``.svg`` file of your choosing.  
Note that the image will be positioned so that the top-left corner will be where your mouse cursor points.

### Changing the Ping
You can modify the ``pointer.js`` file for further options.  
Starting at line 13 you can modify the values to your liking.  
```JS
this.options = {
    key: 88,    // Modify this value to modify the key used.
                // You need to insert a keycode, not the actual key!
                // Example codes: - CTRL = 17
                //                - ALT  = 18
                //                - X    = 88
    // Pointer options
    pointer: {       
        scale: 1,               // Scale relative to the grid_dim
        svg_name: "pointer.svg"     // The svg name
    },
    // Ping options
    ping: {     
        duration: 6,      // Sets the pings duration in seconds
        rotate:   true,      // Toggles Rotation
        rotate_speed: 6,     // Duration in seconds for one complete turn
        size_change:true,           // Toggles Size animation
        size_change_speed: 3,       // Speed for size change animation
                                    // Time for one cycle in seconds
        size_change_amount: 0.125,   // The amount the size changes during one 
                                    // animation cycle
        scale:    1,         // Scales the svg image. The factor is
                                // relative to the grid dimension.
        svg_use:  false,     // Toggles if default ping should be used or a
                                // given svg
        svg_name: "ping.svg" // SVG name. The file has to be located in the
                                // modules repository
                                // The SVG image will be centered on the 
                                // clicked point.
    }
}
```
You can find further keycodes for your keyboard e.g. at https://keycode.info/ .  
**Important:** Changes here will only take effect after reloading  your page. You  do not have to restart the server for the changes to take effect!

## FAQ
**Q: Why does my custom Cursor/Ping image not work?**  
**A:** You have to define the width and height in the ``.svg`` file. For this open the ``.svg`` file with an text editor. Somewhere near the beginning of the file you will see a ``<svg`` tag. Add `` width="256" height="256"`` after it. Make sure to have an empty space in front and after the added tags.  
Sorry for the inconvenience. Sadly most ``.svg`` files do not have these tags defined per default, but the graphics renderer needs this to use these files.  

## Attribution for the provided .svg's

The arrow pointer made by [Iga from the Noun Project](https://thenounproject.com/term/pointer/1727334/) and modified by ayan and me for compatibility.  
The Focus was made by [Creative Stall from the Noun Project](https://thenounproject.com/term/pointer/1727334/).  
Both are licensed under [Creative Commons](https://creativecommons.org/licenses/by/3.0/us/legalcode).

## Compatibility
This module was created and tested for FVTT 0.4.1.  

## Contribution
If you like my work and wand to support me, feel free to leave a tip at my [![paypal](https://img.shields.io/badge/-Paypal-blue.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FYZ294SP2JBGS&source=url).

## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Pointer - a module for Foundry VTT -</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/moerills-fvtt-modules/adnd5e" property="cc:attributionName" rel="cc:attributionURL">Mörill</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).